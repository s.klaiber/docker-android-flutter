FROM ubuntu:latest

ARG ANDROID_SDK_URL="https://dl.google.com/android/repository/tools_r25.2.3-linux.zip"
ARG ANDROID_BUILD_TOOLS="build-tools-27.0.3"
ARG ANDROID_APIS="android-21,android-22,android-23,android-24,android-25,android-27"
ARG ANDROID_EXTRA_PACKAGES="extra-android-m2repository,extra-google-m2repository,extra-google-google_play_services"

RUN useradd -m tns

RUN dpkg --add-architecture i386 && \
    apt-get update && \
    apt-get install -y --no-install-recommends 	apt-transport-https \
						build-essential \
						curl \
						g++ \
						git \
						gradle \
						lib32z1 \
						libc6 \
						lib32ncurses5 \
						lib32stdc++6 \
						maven \
						openjdk-8-jdk \
						software-properties-common \
						unzip \
						usbutils

# ------------------------------------------------------
# --- Download Android SDK tools into $ANDROID_SDK_HOME

RUN mkdir -p /tns /opt/android-sdk

ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64
ENV ANDROID_HOME /opt/android-sdk

RUN curl -o android-sdk.zip ${ANDROID_SDK_URL} && \
    unzip -q android-sdk.zip -d ${ANDROID_HOME} && \
    rm -f android-sdk.zip

RUN echo y | ${ANDROID_HOME}/tools/android update sdk --all --no-ui --filter platform-tools,${ANDROID_BUILD_TOOLS},${ANDROID_APIS},${ANDROID_EXTRA_PACKAGES}

RUN echo y | ${ANDROID_HOME}/tools/bin/sdkmanager --update

RUN yes | ${ANDROID_HOME}/tools/bin/sdkmanager --licenses

ENV PATH ${PATH}:${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools

#RUN /opt/android-sdk/tools/bin/sdkmanager --licenses
#flutter setup


RUN mkdir -p /tns /opt/flutter

RUN cd /opt

RUN git clone https://github.com/flutter/flutter.git && \
    /flutter/bin/flutter doctor && \
    apt-get remove -y curl unzip && \
    apt autoremove -y && \
    rm -rf /var/lib/apt/lists/*

ENV FLUTTER_HOME /opt/flutter

ENV PATH ${PATH}:${FLUTTER_HOME}/bin:${PATH}

RUN /flutter/bin/flutter doctor --android-licenses

WORKDIR /

VOLUME ["/opt/android-sdk-linux"]
